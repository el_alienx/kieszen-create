export default abstract class IDEState {

  private static readonly _data = {
    showConsole: false,
    showExplorer: false,
    showMainWidgets: false,
  }

  // # API
  // Functional
  public static data() {
    return this._data
  }
}