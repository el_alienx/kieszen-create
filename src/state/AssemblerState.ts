// @ts-ignore
import Assembler from 'kieszen-assembler/dist/src/Assembler.js';
import IProjectScript from 'kieszen-core/src/interfaces/IProjectScript';

export default abstract class AssemblerState {
  private static readonly _assembler = new Assembler()

  private static readonly _data = {
    codeCompiled: new Uint8Array
  }

  // # API
  // Pure
  public static data() {
    return this._data
  }

  // Impure, mutate codeCompiled
  public static compile(scripts: IProjectScript[], order: Number[]): Uint8Array {
    const codeSource = this._concatenate(scripts, order)

    return this._assembler.getByteCode(codeSource)
  }

  // # Methods
  // Pure
  private static _concatenate(scripts: IProjectScript[], order: Number[]): string {
    let result = ""

    // sort array based on the scriptOrder
    const newArray = scripts.sort(function (a, b) {
      return order.indexOf(a.id) - order.indexOf(b.id);
    });

    // concatenate based on the new script order
    for (let i = 0; i < newArray.length; i++) {
      const script = newArray[i].content

      result += script + "\n"
    }

    return result
  }

}