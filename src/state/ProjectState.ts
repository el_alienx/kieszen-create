import IProjectScript from 'kieszen-core/src/interfaces/IProjectScript';
import TestImages from 'kieszen-core/src/TestImages';

export default abstract class ProjectState {

  private static readonly _data = {
    project: {
      name: String(),
      version: Number(),
      tree: Array<Object>(),
      scripts: Array<IProjectScript>(),
      spriteSheets: Array<Object>(),
      backgroundSheets: Array<Object>(),
      backgroundMaps: Array<Object>()
    },
    scriptOrder: Array<Number>(),
    selectedId: 0,
  }

  // # Reactive data
  public static data() {
    return this._data
  }

  // # API
  // ## File actions
  public static createScript(id: number) {
    const scripts = this._data.project.scripts
    const newScript: IProjectScript = { id: id, content: "" }

    scripts.push(newScript)
    this._saveProject()
  }

  public static deleteScript(id: number) {
    const scripts = this._data.project.scripts
    const index = scripts.findIndex(script => script.id === id)

    scripts.splice(index, 1)
    this._saveProject()
  }

  public static updateScript(code: string) {
    const scripts = this._data.project.scripts
    const selectedScript = scripts.find(script => script.id === this._data.selectedId)
    const errorMessage = "Script not found to update code"

    if (selectedScript === undefined) throw new Error(errorMessage)
    selectedScript.content = code
    this._saveProject()
  }

  public static updateTree(newTree: Object[]) {
    this._data.project.tree = newTree
    this._saveProject()
  }

  // ## Project actions
  public static downloadProject() {
    const element = document.createElement("a")
    const metadata = "data:text/charset=utf-8"
    const serializedProject = JSON.stringify(this._data.project)
    const encodedProject = encodeURI(serializedProject)

    element.setAttribute("href", `${metadata}, ${encodedProject}`)
    element.setAttribute("download", `${this._data.project.name} ${this._data.project.version}.zen`)
    element.style.display = "none"
    document.body.appendChild(element)

    element.click()
    document.body.removeChild(element)
  }

  public static loadProject() {
    const localFiles = localStorage.getItem("project")

    this._data.project = (localFiles !== null) ? JSON.parse(localFiles) : this.newProject()
  }

  public static newProject(): Object {
    const newScript: IProjectScript = { id: 0, content: "" }

    return {
      name: undefined,
      version: 0,
      tree: [{ id: 0, name: "Main", isLeaf: true }],
      scripts: [newScript],
      backgroundMaps: [],
      spriteSheets: [{ name: "Game sprites", data: TestImages.spriteSheet }],
      backgroundSheets: [{ name: "Game background", data: TestImages.backgroundSheet }],
    }
  }

  public static replaceProject(files: any) {
    const fileReader = new FileReader()

    fileReader.readAsText(files)
    // @ts-ignore
    fileReader.onload = () => this._data.project = JSON.parse(fileReader.result)
  }

  // ## Sheet actions
  public static createSheet(target: string, file: any) {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    const reader = new FileReader();
    const image = new Image()

    canvas.width = 128;
    canvas.height = 128;

    reader.readAsDataURL(file);
    reader.onload = (event) => {
      // @ts-ignore
      image.src = event.target.result
      image.onload = () => {
        // @ts-ignore
        context.drawImage(image, 0, 0)
        // @ts-ignore
        ProjectState.data().project[target].push({ name: file.name, data: canvas.toDataURL() })
        ProjectState._saveProject()
      }
    }
  }

  public static deleteSheet(target: string, index: number) {
    // @ts-ignore
    this._data.project[target].splice(index, 1);
    this._saveProject()
  }

  public static renameSheet(target: string, index: number, newName: string) {
    // @ts-ignore
    this._data.project[target][index].name = newName
    this._saveProject()
  }

  // # Methods
  private static _saveProject() {
    const stringify = JSON.stringify(this._data.project)

    localStorage.setItem("project", stringify)
  }
}
