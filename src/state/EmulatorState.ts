import MemoryMap from 'kieszen-core/src/MemoryMap'
// @ts-ignore
import Emulator from 'kieszen-emulator/dist/src/Emulator.js'

export default abstract class EmulatorState {
  private static _emulator = new Emulator(MemoryMap.bitsAvailable, EmulatorState.updateData)

  private static readonly _data = {
    currentSpriteSheet: 0,
    sectionIndex: 0,
    memory: new Uint8Array(),
    opcode: null,
    registers: {
      A: 0, X: 0, Y: 0, SP: 0, MV: 0, Flags: {
        negative: false,
        overflow: false,
        break: false,
        decimal: false,
        interrupt: false,
        zero: false,
        carry: false
      }
    },
    status: 0,
  }

  // # API
  public static data() {
    return this._data
  }

  public static getDisplay() {
    return EmulatorState._emulator.display
  }

  public static getMemorySection() {
    this._data.memory = this._emulator.getMemorySection(this._data.sectionIndex)
  }

  public static resume() {
    this._emulator.resume()
  }

  public static setROM(compiledCode: Uint8Array, spritesSheets: object[] = [], backgroundSheets: object[] = []) {
    this._emulator.setROM(compiledCode, spritesSheets, backgroundSheets)
  }

  public static stop() {
    this._emulator.stop()
  }

  public static step() {
    this._emulator.step()
  }

  public static start() {
    this._emulator.start()
  }

  public static updateData(data: any) {
    EmulatorState.data().currentSpriteSheet = EmulatorState._emulator.getMemoryData(MemoryMap.hardwareSpriteSheet)
    EmulatorState.data().opcode = data.opcode
    EmulatorState.data().registers = data.registers
    EmulatorState.data().status = data.status
    EmulatorState.getMemorySection()
  }
}