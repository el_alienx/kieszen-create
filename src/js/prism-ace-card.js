Prism.languages.asm6502 = {
	comment: {
		pattern: /[;/].*/,
		alias: "comment"
	},
	xIndex: {
		pattern: /\bX/i,
		alias: "opcode"
	},
	yIndex: {
		pattern: /\bY/i,
		alias: "opcode"
	},
	badImmediate: {
		pattern: /[^\@]\d{4,}/i,
		alias: "bad-immediate"
	},
	badAddress: {
		pattern: /\@\d{6,}/i,
		alias: "bad-address"
	},
	badBinary: {
		pattern: /\%[0-1]{9,}/,
		alias: "bad-binary"
	},
	opcode: {
		pattern: /\b(?:adc|and|asl|bcc|bcs|beq|bit|bmi|bne|bpl|brk|bvc|bvs|clc|cld|cli|clv|cpa|cpx|cpy|dec|dex|dey|eor|inc|inx|iny|jmp|jsr|lda|ldx|ldy|lsr|nop|ora|pha|php|pla|plp|rol|ror|rti|rts|sbc|sec|sed|sei|sta|stx|sty|tax|tay|tsx|txa|txs|tya)\b/i,
		alias: "opcode"
	},
	address: {
		pattern: /\@\d{1,5}/i,
		alias: "address"
	},
	binary: {
		pattern: /\%\_?[0-1]{4}\_?[0-1]{4}/,
		alias: "binary"
	},
	immediate: {
		pattern: /[^\@]\d{1,3}/i,
		alias: "immediate"
	},
	label: {
		pattern: /\w{4,}:/,
		alias: "label"
	},
	badOcode: {
		pattern: /\b[A-Z]{3}\b/i,
		alias: "bad-opcode"
	},
};