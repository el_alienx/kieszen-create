module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `@import "@/styles/style.sass";`
      }
    }
  },
  chainWebpack: config => {
    if (process.env.NODE_ENV === 'development') {
      config
        .output
        .filename('[name].[hash].js') 
        .end() 
    }  
  }
};